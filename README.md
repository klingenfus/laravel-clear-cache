MKS - Laravel Cache Clear 
==========================

Create routes in the project to clear caches.

| Method    | URI                   | Optional                  |
|-----------|-----------------------|---------------------------|
| GET       | /clear/all/{cache?}   | create route cache set /1 |
| GET       | /clear/cache          | -                         |
| GET       | /clear/clear-compiled | -                         |
| GET       | /clear/config         | -                         |
| GET       | /clear/route          | -                         |
| GET       | /clear/route_cache    | -                         |
| GET       | /clear/view           | -                         |


## Installing Laravel Clear Cache

The recommended way to install is through
[Composer](http://getcomposer.org)

 Run the Composer command to install the latest stable version:
```bash
php composer require klingenfus/laravel-clear-cache
```

#### Optional or Laravel Version < 5.4
After an installation, you need configuration providers:
- open the [config/app.php] and add providers
```php
'providers' => [
    ...
    \ClearCache\CodeClear\Providers\CodeClearServiceProvider::class
]
```