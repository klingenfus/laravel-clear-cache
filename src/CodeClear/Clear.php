<?php
    /**
     *
     *
     * @author: Marcel
     */
    
    namespace ClearCache\CodeClear;
    
    use \Illuminate\Support\Facades\Artisan;
    
    class Clear
    {
        protected $commands = [
            'clearCompiled' => 'clear-compiled',
            'cache'         => 'cache:clear',
            'view'          => 'view:clear',
            'config'        => 'config:clear',
            'route'         => 'route:clear',
            'route_cache'   => 'route:cache',
        ];
        
        /**
         * Remove bootstrap/cache - File : compiled.php / services.php
         *
         * @return mixed
         */
        public function clearCompiled()
        {
            Artisan::call($this->commands['clearCompiled']);
            return $this->readView('Clear Compiled');
        }
        
        /**
         * Clear Cache facade value
         *
         * @return mixed
         */
        public function cache()
        {
            Artisan::call($this->commands['cache']);
            return $this->readView('Cache Clear');
        }
        
        /**
         * Clear View cache
         *
         * @return mixed
         */
        public function view()
        {
            Artisan::call($this->commands['view']);
            return $this->readView('View Clear');
        }
        
        /**
         * Clear Config cache
         *
         * @return mixed
         */
        public function config()
        {
            Artisan::call($this->commands['config']);
            return $this->readView('Config Clear');
        }
        
        /**
         * Clear Route cache
         *
         * @return mixed
         */
        public function route()
        {
            Artisan::call($this->commands['route']);
            return $this->readView('Route Clear');
        }
        
        /**
         * Route cache
         *
         * @return mixed
         */
        public function routeCache()
        {
            Artisan::call($this->commands['route_cache']);
            return $this->readView('Route Cache');
        }
        
        /**
         * Clear All cache and cache Route
         *
         * @return mixed
         */
        public function all($cache = null)
        {
            $comands = $this->commands;
            
            if ($cache == null) {
                unset($comands['route_cache']);
                $msg = 'All clear - no route cache';
            } else {
                $msg = 'All clear and route cache';
            }
            
            foreach ($comands as $comand) {
                Artisan::call($comand);
            }
            
            return $this->readView($msg);
        }
        
        /**
         *
         */
        protected function readView($msg)
        {
            return view('code_clear::clear', compact('msg'));
        }
    }