<?php
    /**
     *
     * @author: Marcel
     */
    
    namespace ClearCache\CodeClear\Providers;
    
    use ClearCache\CodeClear\Clear;
    use Illuminate\Support\ServiceProvider;
    
    class CodeClearServiceProvider extends ServiceProvider
    {
        protected $dir     = __DIR__;
        protected $dir_sep = DIRECTORY_SEPARATOR;
        
        protected function setPath(...$path)
        {
            array_unshift($path, $this->dir);
            return implode($this->dir_sep, $path);
        }
        
        public function boot()
        {
            $this->loadViewsFrom($this->setPath('..', '..', 'resources', 'views'), 'code_clear');
            $this->loadRoutesFrom($this->setPath('..', 'routes.php'));
        }
        
        public function register()
        {
        }
    }