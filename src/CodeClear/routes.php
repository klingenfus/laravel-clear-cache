<?php
    Route::group([
        //            'middleware' => [
        //                'auth',
        //            ],
        'prefix' => 'clear',
        'as'     => 'clear.',
    ], function () {
        Route::get('/clear-compiled', ['uses' => 'ClearCache\CodeClear\Clear@clearCompiled']);
        Route::get('/cache', ['uses' => 'ClearCache\CodeClear\Clear@cache']);
        Route::get('/view', ['uses' => 'ClearCache\CodeClear\Clear@view']);
        Route::get('/config', ['uses' => 'ClearCache\CodeClear\Clear@config']);
        Route::get('/route', ['uses' => 'ClearCache\CodeClear\Clear@route']);
        Route::get('/route_cache', ['uses' => 'ClearCache\CodeClear\Clear@routeCache']);
        Route::get('/all/{cache?}', ['uses' => 'ClearCache\CodeClear\Clear@all']);
    });
