<?php
    /**
     *
     *
     * @author: Marcel
     */
    
    namespace ClearCache\CodeClear;
    
    
    use PHPUnit\Framework\TestCase;
    
    class ClearTest extends TestCase
    {
        public function createApplication()
        {
            $app = require __DIR__.'/../bootstrap/app.php';
            
            $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();
            
            $this->clearCache(); // NEW LINE -- Testing doesn't work properly with cached stuff.
            
            return $app;
        }
        
        protected function clearCache()
        {
            $commands = ['clear-compiled', 'cache:clear', 'view:clear', 'config:clear', 'route:clear'];
            foreach ($commands as $command) {
                \Illuminate\Support\Facades\Artisan::call($command);
            }
        }
    }